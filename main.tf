resource "random_string" "serviceaccount" {
  length = 8
  upper = false
  special = false
}

resource "kubernetes_service_account_v1" "main" {
  metadata {
    name = "${var.service_account}-${random_string.serviceaccount.id}"
    namespace = var.k8s_namespace
  }
}

resource "kubernetes_token_request_v1" "main" {
  depends_on = [
    kubernetes_service_account_v1.main
  ]
  metadata {
    name = kubernetes_service_account_v1.main.metadata.0.name
    namespace = var.k8s_namespace
  }
  spec {
    audiences = [
      "api"
    ]
  }
}

data "gitlab_project" "main" {
  path_with_namespace = var.gitlab_repository
}

resource "gitlab_project_environment" "main" {
  project = data.gitlab_project.main.id
  name = var.repo_environment
  stop_before_destroy = true
}

resource "gitlab_project_variable" "kubeconfig" {
  depends_on = [
    kubernetes_service_account_v1.main,
    kubernetes_token_request_v1.main
  ]

  project = data.gitlab_project.main.id
  environment_scope = gitlab_project_environment.main.slug
  variable_type = "file"
  key = "KUBECONFIG"
  value = templatefile("${path.module}/templates/kubeconfig.yaml", {
    k8s_ca_data = var.k8s_ca_data,
    k8s_server = var.k8s_server,
    service_account_name = kubernetes_service_account_v1.main.metadata.0.name,
    service_account_token = kubernetes_token_request_v1.main.token,
    k8s_clustername = var.k8s_clustername
  })
}
