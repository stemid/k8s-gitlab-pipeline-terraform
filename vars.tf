variable "gitlab_repository" {
  type = string
}

variable "repo_environment" {
  type = string
}

variable "service_account" {
  type = string
}

variable "k8s_namespace" {
  type = string
}

# TODO: Figure out a way to get these as data values from the cluster.
variable "k8s_ca_data" {
  type = string
}

variable "k8s_server" {
  type = string
}

variable "k8s_clustername" {
  type = string
}
