# Kubernetes Gitlab pipeline

Terraform module for internal use.

Sets up the necessary ServiceAccount in kubernetes, and CI/CD variables in a Gitlab project to run deployment pipelines in a specific namespace.

## Example

```yaml
module "pipeline" {
  source = "git::https://gitlab.com/stemid/k8s-gitlab-pipeline"
  count = length(var.gitlab_repositories)
  gitlab_repository = var.gitlab_repositories[count.index].name
  service_account = var.gitlab_repositories[count.index].service_account
  repo_environment = var.repo_environment
  k8s_namespace = var.k8s_namespace
  k8s_server = var.k8s_server
  k8s_clustername = var.k8s_clustername
  k8s_ca_data = var.k8s_ca_data
}
```
