terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "16.6.0"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "2.30.0"
    }
    random = {
      source = "hashicorp/random"
      version = "3.5.1"
    }
  }
  required_version = ">= 1.5.1"
}
